<?php
// Es un documento que or medio de PHP puede ser manipulado 

//Una lista de parámetros opentype para la función fopen son:
//
//"r" : abrir un archivo para lectura, el fichero debe existir.
//"w" : abrir un archivo para escritura, se crea si no existe o se sobreescribe si existe.
//"a" : abrir un archivo para escritura al final del contenido, si no existe se crea.
//"a+" : abrir un archivo para escritura al final del contenido, si no existe se crea.
//"r+" : abrir un archivo para lectura y escritura, el fichero debe existir.
//"w+" : crear un archivo para lectura y escritura, se crea si no existe o se sobreescribe si existe.
//"r+b ó rb+" : Abre un archivo en modo binario para actualización (lectura y escritura).
//"rb" : Abre un archivo en modo binario para lectura.

$r = "archivo_file.txt";
$m = "a+";
$archivo = fopen($r, $m); // Aqui abrimos el archivo 

// Final de archivo 
//while(!feof($archivo)){
//    $contenido = fgets($archivo); // que obtenga el contenido del archivo 
//    echo "$contenido<br>";
//}

//$srtin = "<br>Estudiantes de 3 de programación Visual Matutina<br>";
////
//fwrite($archivo, $srtin); // Vamos a escribir dentro del archivo  

//Cerrar un fichero
//fclose($archivo);


//copiar archivo
//copy("archivo_file.txt", "archivo_copiado.txt");

// renombrar el archivo
//rename("archivo_copiado.txt", "archivo_renombrado.txt");

// eliminar el archivo
unlink("archivo_renombrado.txt");








