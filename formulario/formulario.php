<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Que es session.- Una variable superglobal que almacena y persiste lo datos a lo 
// largo de la navegacion del sitio web, hasta que son cerrados o la variable es destruida

// requisitos
// 1.- debe tener en cada una de sus páginas en las cuales va a navegar el session_start();
// 2.- las variables se las construye de la siguiente forma:
//      $_SESSION['nombre de la variable ']
session_start();
?>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>SG-PGSESIONES</title>
    </head>
    <body>
        <h1>Formulario en PHP</h1>
        <form method="GET" action="recibir.php">
            <p>
                <label>Nombre</label>
                <input type="text" id="nombre" name="nombre">
            </p> 
            <p>
                <label>Apellido</label>
                <input type="text" id="apellido" name="apellido">
            </p>
            <p>
                <label>Cédula</label>
                <input type="text" id="cedula" name="cedula">
            </p>
            <input type="submit" value="Enviar Info"><br>
        </form>
<!--        Tipos de input
        
            <label>Numero</label>
            <input type="number" id="cedula" name="cedula"><br>
            <label>Fecha</label>
            <input type="date" id="cedula" name="cedula"><br>
            <label>Fecha y hora</label>
            <input type="datetime" id="cedula" name="cedula"><br>
            <label>Archivo</label>
            <input type="file" id="cedula" name="cedula"><br>
            <label>Radio botton</label>
            <input type="radio" id="cedula" name="cedula"><br>
            <label>Checkbox</label>
            <input type="checkbox" id="cedula" name="cedula"><br>
            <label>Oculto</label>
            <input type="hidden" id="cedula" name="cedula"><br>-->
   <?php
        echo $_SESSION['variableSession']."<br>";
        echo $varableNormal;
    ?>
    <a href="logout.php">Cerrar Sesion</a>
    </body>    
</html>

