<?php
 
 
 $errores = array();
 
 if (isset($_POST)){
     include './includes/conexion.php';
     
     if(!isset($_SESSION)){
            session_start();
        }
        
     // instanciamos lo que nos viene por POST
    $nombre = isset($_POST['nombre']) ? $_POST['nombre']:false;
    $apellido = isset($_POST['apellidos']) ? $_POST['apellidos']:false;
    $email = isset($_POST['email']) ? $_POST['email']:false;
    $password = isset($_POST['password']) ? $_POST['password']:false;
    
    // validar los elementos 
       // difrente de vacio  diferente de numero   no contenga caracteres del 0-9
    if (!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre no es valido";
    }
    if (!empty($apellido) && !is_numeric($apellido) && !preg_match("/[0-9]/", $apellido)){
        $apellido_valido = true;
    }else{
        $apellido_valido = false;
        $errores['apellidos'] = "Los Apellidos no son valido";
    }
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)){
        $correo_valido = true;
    }else{
        $correo_valido = false;
        $errores['email'] = "El correo no es valido";
    }
    if (!empty($nombre)){
        $password_valido = true;
    }else{
        $password_valido = false;
        $errores['password'] = "El password esta vacio";
    }
     $guardar_registro = false;
     if (count($errores) == 0){
         $guardar_registro = true;
         // cifrar la contraseña 
         $password_segura = password_hash($password, PASSWORD_BCRYPT, ['cost' => 4]); 
         $sql = "insert into usuarios values(null, '$nombre','$apellido','$email','$password_segura',curdate())";
         $guardar = mysqli_query($db, $sql);
         if(guardar){
             $_SESSION['completado'] = "El registro de usuario se grabo con exito";
         }else{
             $_SESSION['errores']['general'] = "Fallo al guardar el registro ";
         }
     }else{
         $_SESSION['errores'] = $errores;
     }
     header('Location: index.php');
 }

