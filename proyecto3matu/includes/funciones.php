<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function mostrarError($errores, $campo){
    $alerta="";
    if(isset($errores[$campo]) && !empty($campo)){
        $alerta ="<div class='alerta alerta-error'>".$errores[$campo]."</div>";
    }
    return $alerta;
}
function conseguirCategorias($conexion){
    $sql = "select * from categorias order by id asc";
    $categorias = mysqli_query($conexion, $sql);
    $resultado = array();
    if($categorias && mysqli_num_rows($categorias) >= 1){
        $resultado = $categorias;
    }
    return $resultado;
}

