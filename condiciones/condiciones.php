<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//El constructor if es una de las características más importantes de muchos lenguajes, incluido PHP. Permite la ejecución condicional
//de fragmentos de código. PHP dispone de una estructura if que es similar a la de C:
//if (condicion){
//    codigo a ejecutarse si la condición es correcta 
//}esle{
//    si la condiciones es incorrecta
//}
//if (condicion):
//        odigo a ejecutarse si la condición es correcta 
//esle:
//    si la condiciones es incorrecta
//endif 
// isset.- si existe esa variable
// empty.- si esta vacio 
// is_null.- es nullo
// Operadores de comparación 
// ==   Nos indica que los valores entre variables son iguales
// ===  Nos indica que los valores entre variables son iguales como tambien el tipo 
// !=   Nos indica que los valores entre variables son diferentes 
// !=== NOs indica que los valores entre variables son diferentes como los tipos 
// >    Nos indica que una variable es mayor que la otra 
// <    Nos indica que una variable es menor que la otra 
// <=   Nos indica que una de las variables es nenor o igual a la otra variable
// >=   Nos indica que una de las variables es mayor o igual a la otra variable

// Operadores Lógicos
// &&  and Nos permite compara dos condiciones y va de la siguiente manera :
//            v   &&  v  =  v
//            v   &&  f  =  f
//            f   &&  v  =  f
//            f   &&  f  =  f

// ||  or Nos permite compara dos condiciones y va de la seiguiente manera:
//            v   ||  v  =  v
//            v   ||  f  =  v
//            f   ||  v  =  v
//            f   ||  f  =  f


echo "<h1>Condicion IF</h1>";


$edad = $_GET['edad'];
if (empty($edad)){
    echo "No tiene ninguna edad";
}else{
    if ($edad >= 18):
        echo "La persona es mayor de edad";
    else:
        echo "La persona es menor de edad";
    endif;
}

echo "<br>";
    echo "<h1>Condicion ELSE IF</h1>";
    $color = $_GET['color'];
    if ($color == 1){
        echo "El color es gris";
    }else if($color == 2){
        echo "El color es azul";
    }else if($color == 3){
        echo "El color es verde";
    }else if($color == 4){
        echo "El color es amarillo";
    }else{
        echo "No elegiste el color";
    }
echo "<br>";
    echo "<h1>Condicion Switch</h1>";
    if (isset($color)){
        switch ($color){
            case 1:
                echo "El color es gris";
            break;
            case 2:
                echo "El color es azul";
            break;
            case 3:
                echo "El color es verde";
            break;
            case 4:
                echo "El color es amarillo";
            break;
        }
    }
//La sentencia switch es similar a una serie de sentencias IF en la misma expresión. 
//En muchas ocasiones, es posible que se quiera comparar la misma variable (o expresión) con muchos valores 
//diferentes, y ejecutar una parte de código distinta dependiendo de a que valor es igual. Para esto es exactamente 
//la expresión switch.

    
//Ejemplos con operadores
    
// Puede votar si es mayor de 16 años y es ecuatoriano
echo "<br>";
    echo "<h1>Ejemplo con condiciones</h1>";  
$nacionalidad = $_GET['nac'];
            if (($edad >= 16 and $edad <=65) && ($nacionalidad == "E" || 
                $nacionalidad == "V" || $nacionalidad == "C" || $nacionalidad == "A")){
                echo "El ciudadano de america latina puede votar";
            }else{
                echo "El ciudadano de america latina NO puede votar ";
            }
 
            
// Si es mayor de edad, es parte del ISTJBA y es de Daule, imprimir en pantalla 
// que es un aspirante a nuevo tecnólogo 

